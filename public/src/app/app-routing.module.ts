import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdministradorComponent } from './administrador/administrador.component';
import { AgendarComponent } from './gestor/agendar/agendar.component';
import { GestorComponent } from './gestor/gestor.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';
import { LoginComponent } from './login/login.component';
import { MedicoComponent } from './medico/medico.component';
import { PacienteComponent } from './paciente/paciente.component';
import { AyudaComponent } from './ui/ayuda/ayuda.component';
import { UsuarioComponent } from './ui/usuario/usuario.component';

const routes: Routes = [
  {path: '', redirectTo: '/', pathMatch: 'full'},
  {path: '', component: LoginComponent, canActivate: [ LoginGuard ]},
  {
    path: 'gestor', component: GestorComponent, canActivate: [ AuthGuard ], children: [
      {path: '', redirectTo: '', pathMatch: 'full'},
      {path: 'usuario', component: UsuarioComponent},
      {path: 'agendar', component: AgendarComponent},
      {path: 'ayuda', component: AyudaComponent},
    ],
  },
  {path: 'paciente', component: PacienteComponent, canActivate: [ AuthGuard ]},
  {path: 'administrador', component: AdministradorComponent, canActivate: [ AuthGuard ]},
  {path: 'medico', component: MedicoComponent, canActivate: [ AuthGuard ]},
  {path: '**', redirectTo: '/', pathMatch: 'full'},

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
})
export class AppRoutingModule {
}
