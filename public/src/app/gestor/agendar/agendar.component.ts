import { Component, OnInit } from '@angular/core';
import { PacienteService } from '../../services/paciente/paciente.service';

@Component({
  selector: 'app-gestor-agendar',
  templateUrl: './agendar.component.html',
  styleUrls: [ './agendar.component.scss' ],
})
export class AgendarComponent implements OnInit {
  public typeOfDocuments: object[];
  public typeOfServices: object[];
  public numberDocument: string;
  public foundPatient: boolean;
  public typeDocument: string;
  public patientFound: string;
  private patients: object;

  constructor (private _patient: PacienteService,
  ) {
  }

  ngOnInit () {
    this.initData();
    this.patients = this._patient.getPacientes();
  }

  /**
   * Initialize data in component
   */
  initData () {
    this.numberDocument = '';
    this.patientFound = '';
    this.foundPatient = true;
    this.typeOfDocuments = [
      {value: 0, view: 'Tarjeta de Identidad'},
      {value: 1, view: 'Cedula de Ciudadania'},
      {value: 2, view: 'Pasaporte'},
    ];
    this.typeOfServices = [
      {value: 0, view: 'Consulta Externa'},
      {value: 1, view: 'Laboratorio'},
      {value: 1, view: 'Especialista'},
    ];
  }

  /**
   * Where clicked button "search" in component gestor
   */
  public search (): void {
    const patientFound = Object.values(this.patients).filter(
      patient => patient.numberDocument === this.numberDocument,
    );
    if (patientFound.length > 0) {
      this.patientFound = `${patientFound[ 0 ].firstName} ${patientFound[ 0 ].secondName}`;
      this.foundPatient = false;
    } else {
      this.patientFound = '';
      this.foundPatient = true;
    }

  }

}
