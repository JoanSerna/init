import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../interfaces/user';
import { LoginService } from '../services/login/login.service';

@Component({
  selector: 'app-gestor',
  templateUrl: './gestor.component.html',
  styleUrls: [ './gestor.component.scss' ],
  animations: [
    trigger('agendarAnimation', [
      state('hidden', style({
        opacity: '0',
      })),
      state('visible', style({
        opacity: '1',
      })),
      transition('hidden <=> visible', animate('250ms ease-in')),
    ]),
  ],
})
export class GestorComponent implements OnInit {
  public state: string;
  public rol: number;
  public user: Object[];
  protected clickedSchedule: boolean;
  protected clickedSearch: boolean;
  protected clickedHelp: boolean;

  /**
   * Dependency Injection
   * @param {Router} _router
   * @param _loginService
   */
  constructor (
    private _router: Router,
    private _loginService: LoginService,
  ) {

    this.initData();
  }

  ngOnInit (): void {

  }

  /**
   * Initialize data in component
   */
  private initData (): void {
    this.state = 'hidden';
    this.clickedHelp = false;
    this.clickedSchedule = false;
    this.clickedSearch = false;
    const userLoggued: User = this._loginService.userData;
    this.rol = userLoggued.rol_id;
    this.user = [
      {
        text: `${userLoggued.nombres} ${userLoggued.apellido}`,
        icon: 'account_circle',
      },
      {
        text: userLoggued.correo,
        icon: 'email',
      },
      {
        text: userLoggued.celular,
        icon: 'phone',
      },
      {
        text: userLoggued.fecha_nacimiento,
        icon: 'date_range',
      },
    ];
  }

  /**
   * Where clicked button "Inicio" in Component Aside
   * @param clicked
   */
  public goHome (clicked): void {
    this.state = 'hidden';
    setTimeout(() => {
      this._router.navigate([ 'gestor' ]);
    }, 500);

    this.clickedSchedule = false;
    this.clickedHelp = false;
    this.clickedSearch = false;
  }

  /**
   * Where clicked button "Agendar" in component Aside
   * @param {boolean} clicked
   */
  public goSchedule (clicked: boolean): void {
    if (this.state === 'visible') {
      this.state = 'hidden';
      setTimeout(() => {
        this._router.navigate([ 'gestor/agendar' ]);
        this.state = 'visible';
      }, 500);
    } else {
      this.state = 'visible';
      this._router.navigate([ 'gestor/agendar' ]);
    }
  }

  /**
   * Where clicked button "Usuario" in component header
   * Transitions between components
   * @param {boolean} clicked
   */
  public goUser (clicked: boolean) {
    if (this.state === 'visible') {
      this.state = 'hidden';
      setTimeout(() => {
        this._router.navigate([ 'gestor/usuario' ]);
        this.state = 'visible';
      }, 250);
    } else {
      this.state = 'visible';
      this._router.navigate([ 'gestor/usuario' ]);
    }
  }

  /**
   * Where clicked button "Ayuda" in component Header
   * @param {boolean} clicked
   */
  public goHelp (clicked: boolean): void {
    if (this.state === 'visible') {
      this.state = 'hidden';
      setTimeout(() => {
        this._router.navigate([ 'gestor/ayuda' ]);
        this.state = 'visible';
      }, 250);
    } else {
      this.state = 'visible';
      this._router.navigate([ 'gestor/ayuda' ]);
    }
  }

  /**
   * Where Clicked button "Cerrar Session" in component Header
   * @param user
   */
  logout (user): void {
    sessionStorage.clear();
    this._router.navigate([ '/' ]);
  }
}
