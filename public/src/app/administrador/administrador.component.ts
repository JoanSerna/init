import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, ThemePalette } from '@angular/material';
import { Router } from '@angular/router';
import { User } from '../interfaces/user';
import { ConfigService } from '../services/config/config.service';
import { LoginService } from '../services/login/login.service';

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: [ './administrador.component.scss' ],
})
export class AdministradorComponent implements OnInit {
  public rol: number;
  public datas: Object[];
  public listDoc: object[];
  public schedule: boolean;
  public BuscarPers: boolean;
  public crear: boolean;
  public crearGest: boolean;
  public reportes: boolean;
  public configure: boolean;
  public cita_general;
  primary: ThemePalette;
  public formConfiguracionSistema: FormGroup;

  constructor (
    private _router: Router,
    public dialog: MatDialog,
    private _loginService: LoginService,
    private _fb: FormBuilder,
    private _configService: ConfigService) {

    this.formConfiguracionSistema = _fb.group({
      duracion_citageneral: [ null, Validators.required ],
      duracion_cita_especialista: [ null, Validators.required ],
      duracion_cita_laboratorio: [ null, Validators.required ],
      hora_inicio: [ null, Validators.required ],
      ultima_hora: [ null, Validators.required ],
    });

    this.reportes = false;
    this.primary = 'primary';
    const userLoggued: User = this._loginService.userData;
    this.rol = userLoggued.rol_id;
    this.datas = [
      {
        text: `${userLoggued.nombres} ${userLoggued.apellido}`,
        icon: 'account_circle',
      },
      {
        text: userLoggued.correo,
        icon: 'email',
      },
      {
        text: userLoggued.celular,
        icon: 'phone',
      },
      {
        text: userLoggued.fecha_nacimiento,
        icon: 'date_range',
      },
    ];
    this.schedule = false;
  }

  ngOnInit () {
    this.listDoc = [
      {value: 'Cedula de ciudania-0', ViewValue: 'Cedula de ciudania'},
      {value: 'Tarjeta de identidad-1', ViewValue: 'tarjena de identidad'},
      {value: 'Cedula de extranjeria-2', ViewValue: 'Cedula de ectranjeria'},
    ];
  }

  logout (user) {
    sessionStorage.clear();
    this._router.navigate([ '/' ]);
  }

  asideBuscar (parame) {
    this.configure = false;
    this.BuscarPers = parame;
    this.crear = false;
    this.crearGest = false;
    this.reportes = false;
  }

  asideCrear (evenMostrar) {
    this.configure = false;
    this.BuscarPers = false;
    this.crear = evenMostrar;
    this.crearGest = false;
    this.reportes = false;
  }

  asideCrearG (evenMostrar) {
    this.configure = false;
    this.BuscarPers = false;
    this.crear = false;
    this.crearGest = evenMostrar;
    this.reportes = false;
  }

  asideRport (evenMost) {
    this.configure = false;
    this.BuscarPers = false;
    this.crear = false;
    this.crearGest = false;
    this.reportes = evenMost;
  }

  asideConfig (eventMos) {
    this.configure = eventMos;
    this.BuscarPers = false;
    this.crear = false;
    this.crearGest = false;
    this.reportes = false;
  }

  goHome (data) {
    this.configure = false;
    this.BuscarPers = false;
    this.crear = false;
    this.crearGest = false;
    this.reportes = false;
  }

  addConfig (config: FormGroup) {
    const user: User = this._loginService.userData;
    config.value.empresa_ips_id = user.empresa_ips_id;
    this._configService.addConfigSystem(config.value).subscribe(response => console.log(response), error => console.error(error));
  }
}

// logout(user) {
//     sessionStorage.clear();
//     this._router.navigate(['/']);
// }
