import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AdministradorComponent } from './administrador/administrador.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgendarComponent } from './gestor/agendar/agendar.component';
import { GestorComponent } from './gestor/gestor.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';
import { LoginComponent } from './login/login.component';
import { MaterialModule } from './material/material.module';
import { MedicoComponent } from './medico/medico.component';
import { PacienteComponent } from './paciente/paciente.component';
import { ConfigService } from './services/config/config.service';
import { EmailService } from './services/email/email.service';
import { LoginService } from './services/login/login.service';
import { MedicoService } from './services/medico/medico.service';
import { PacienteService } from './services/paciente/paciente.service';
import { AsideComponent } from './ui/aside/aside.component';
import { AyudaComponent } from './ui/ayuda/ayuda.component';
import { FooterComponent } from './ui/footer/footer.component';
import { HeaderComponent } from './ui/header/header.component';
import { UsuarioComponent } from './ui/usuario/usuario.component';

@NgModule({
  declarations: [
    AppComponent,
    GestorComponent,
    LoginComponent,
    HeaderComponent,
    AsideComponent,
    FooterComponent,
    PacienteComponent,
    AdministradorComponent,
    MedicoComponent,
    AyudaComponent,
    AgendarComponent,
    UsuarioComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),
  ],
  providers: [
    LoginService,
    AuthGuard,
    LoginGuard,
    PacienteService,
    EmailService,
    MedicoService,
    ConfigService,
  ],
  bootstrap: [ AppComponent ],
})
export class AppModule {
}
