import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {

  constructor (private _http: HttpClient) {
  }

  addConfigSystem (value) {
    console.log(value);
    return this._http.post(`${environment.apiPycloudmeDev}config`, value);
  }
}
