import { Injectable } from '@angular/core';
import { Rol, User, TypeDocument } from '../../interfaces/user';
@Injectable()
export class PacienteService {
  private pacientes: object[];
  constructor() {
    this.pacientes = [
      {
        typeDocument: TypeDocument.CedulaCiudadania,
        rol: Rol.Paciente,
        numberDocument: 1112793351,
        firstName: 'Robert el gay',
        secondName: 'chico mariposa',
        firstLastname: 'Firt lastname',
        secondLastname: 'second lastname',
        email: 'robert@gay.negros',
        birthDate: '01/01/1999',
        phoneFixed: '212793',
        phoneNumber: '3177272074',
        oldEPS: 'SOS'
      },
      {
        typeDocument: TypeDocument.CedulaCiudadania,
        rol: Rol.Paciente,
        numberDocument: 1112793361,
        firstName: 'joan',
        secondName: 'manuel',
        firstLastname: 'serna',
        secondLastname: 'leiton',
        email: 'jsernaleiton@gmail.com',
        birthDate: '01/01/1999',
        phoneFixed: '212793',
        phoneNumber: '3177272074',
        oldEPS: 'SOS'
      },
      {
        typeDocument: TypeDocument.CedulaCiudadania,
        rol: Rol.Paciente,
        numberDocument: 1112791361,
        firstName: 'yenifer',
        secondName: 'alejandra',
        firstLastname: 'valencia',
        secondLastname: 'rincon',
        email: 'yenifer@rincon.com',
        birthDate: '01/01/1999',
        phoneFixed: '212793',
        phoneNumber: '3177272074',
        oldEPS: 'SOS'
      },
      {
        typeDocument: TypeDocument.CedulaCiudadania,
        rol: Rol.Paciente,
        numberDocument: 1112893361,
        firstName: 'daniela',
        secondName: 'maria',
        firstLastname: 'gonzalez',
        secondLastname: 'gonzalez',
        email: 'daniela@gonzales.com',
        birthDate: '01/01/1999',
        phoneFixed: '212793',
        phoneNumber: '3177272074',
        oldEPS: 'SOS'
      },
      {
        typeDocument: TypeDocument.CedulaCiudadania,
        rol: Rol.Paciente,
        numberDocument: 1112793161,
        firstName: 'joseph',
        secondName: 'antonio',
        firstLastname: 'perez',
        secondLastname: 'correa',
        email: 'vergalarga@josep.com',
        birthDate: '01/01/1999',
        phoneFixed: '212793',
        phoneNumber: '3177272074',
        oldEPS: 'SOS'
      }
    ];
  }

  /**
   * Get All Patients
   * @returns {object[]}
   */
  getPacientes(): object[] {
    return this.pacientes;
  }
}
