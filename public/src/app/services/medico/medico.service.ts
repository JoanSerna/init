import { Injectable } from '@angular/core';
import { TypeDocument } from '../../interfaces/user';
@Injectable()
export class MedicoService {
  private medicos: object[];
  constructor() { }

  getMedico(): object[] {
    return this.medicos = [
      {
        propiedad: 'Tipo de documento',
        valor: TypeDocument.CedulaCiudadania
      },
      {
        propiedad: 'Numero de documento',
        valor: 1004798195,
      },
      {
        propiedad: 'Primer nombre',
        valor: 'Joseph',
      },
      {
        propiedad: 'Segundo nombre',
        valor: '',
      },
      {
        propiedad: 'Primer apellido',
        valor: 'Ochoa',
      },
      {
        propiedad: 'Segundo apellido',
        valor: 'Gonzalez',
      },
      {
        propiedad: 'Fecha de nacimiento',
        valor: '01/02/2000',
      },
      {
        propiedad: 'Numero telefonico',
        valor: '2102775',
      },
      {
        propiedad: 'Numero de Celular',
        valor: '3214608816',
      },
      {
        propiedad: 'Profecion',
        valor: 'Medico',
      },
      {
        propiedad: 'email',
        valor: 'Josephhhochoa@hotmail.com',
      },
      {
        propiedad: 'Especialidad',
        valor: 'Ginecologo',
      },
      {
        propiedad: 'Especialidad 2',
        valor: 'Obtometrista',
      },
      {
        propiedad: 'Especialidad 3',
        valor: 'Medico General',
      },
      {
        propiedad: 'Jornada',
        valor: 'Tarde',
      },
      {
        propiedad: 'Departamento',
        valor: 'Internista',
      }
    ];
  }

}
