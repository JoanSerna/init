import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.scss']
})
export class PacienteComponent implements OnInit {
    public rol: string;
    public datas: object[];
    public servicios: object[];
    public listaProfessi: object[];
    public listaEspecialis: object[];
    public listaFecha: object[];
    public listHora: object[];
    public agendar: boolean;
    public cancelacion: boolean;
    public schedule: boolean;

    constructor(private _router: Router, public dialog: MatDialog) {
        this.datas = [
            {
                text: 'Yenifer Valencia',
                icon: 'account_circle'
            },
            {
                text: 'yenifer.valenciar@gmail.com',
                icon: 'email'
            },
            {
                text: '3218291685',
                icon: 'phone'
            },
            {
                text: '19/03/1194',
                icon: 'date_range'
            }
        ];
    }

    logout(user) {
        sessionStorage.clear();
        this._router.navigate(['/']);
    }

    asideAgen(evenMostrar) {
        this.cancelacion = evenMostrar;
        this.agendar = !this.agendar;
        this.cancelacion = !this.cancelacion;
    }

    asideCancelar(evenMostra) {
        this.agendar = evenMostra;
        this.agendar = !this.agendar;
        this.cancelacion = !this.cancelacion;
    }
    goHome(data) {
        this.schedule = !data;
    }
  ngOnInit() {
    const typeRol = sessionStorage.getItem('key');
    this.rol = typeRol.substr(0, 1);
    this.servicios = [
      { value: 'general-0', ViewValue: 'Medico General' },
      { value: 'especialista-1', ViewValue: 'Especialista' },
      { value: 'laboratirio-2', ViewValue: 'Laboratorio' }
    ];
    this.listaProfessi = [
        { value: 'Joan Menuel Sena-0', ViewValue: 'Joan Manuel Serna' },
        { value: 'Robert Sty Triana-1', ViewValue: 'Robert Sty Triana' },
        { value: 'Yenifer valencia-2', ViewValue: 'Yenifer Valencia' }
    ];
    this.listaEspecialis = [
        { value: 'Nefrologia-0', ViewValue: 'Nefrologia' },
        { value: 'Otorrinologia-1', ViewValue: 'Otorrinologia' },
        { value: 'Pediatria-2', ViewValue: 'Pediatria' }
    ];
    this.listaFecha = [
        {value: '29-Abril-2018-0', ViewValue: '29-Abril-2018'},
        {value: '05-Mayo-2018-1', ViewValue: '05-Mayo-2018'},
        {value: '12-Mayo-2018-2', ViewValue: '12-Mayo-2018'}
    ];
    this.listHora = [
        {value: '08:00am-0', ViewValue: '08:00am'},
        {value: '08:30am-0', ViewValue: '08:30am'},
        {value: '05:30pm-0', ViewValue: '05:30pm'}
    ];
  }

    // openDialog(): void {
    //     let dialogRef = this.dialog.open( {
    //         width: '250px',
    //     });
    // };
}
