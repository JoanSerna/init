import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Auth, ResponseServer, Rol, User } from '../interfaces/user';
import { LoginService } from '../services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.scss' ],
})
export class LoginComponent {
  public typeOfDocuments: object[];
  public form: FormGroup;
  private userResponse;

  constructor (
    private _login: LoginService,
    private _snack: MatSnackBar,
    private _router: Router,
    private _fb: FormBuilder,
  ) {
    this.form = _fb.group({
      tpd_document_id: [ null, Validators.required ],
      number_document: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
        ]),
      ],
      nickname: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
        ]),
      ],
      password: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(50),
        ]),
      ],
    });
    this.typeOfDocuments = [
      {value: 1, view: 'Tarjeta de Identidad'},
      {value: 2, view: 'Cédula de Ciudadanía'},
      {value: 3, view: 'Pasaporte'},
    ];
  }

  onLogin (userLogin: Auth): void {
    console.log(userLogin);
    this._login.getUser(userLogin)
      .subscribe((response: ResponseServer) => {
        const user: User = response.data;
        console.log(user);
        if (user.nickname) {
          this.userResponse = user;
          this._login.logged(true);
          this._login.userData = user;
          switch (user.rol_id) {
            case Rol.Gestor:
              this._router.navigate([ 'gestor' ]);
              break;
            case Rol.Paciente:
              this._router.navigate([ 'paciente' ]);
              break;
            case Rol.Medico:
              this._router.navigate([ 'medico' ]);
              break;
            case Rol.Administrador:
              this._router.navigate([ 'administrador' ]);
              break;
            default:
              this._router.navigate([ '/' ]);
          }
        } else {
          console.log(response);
          console.log('No existe el usuario');
          this._snack.open('Usuario o contraseña incorrectos', 'Cerrar', {
            duration: 2000,
          });
        }
      }, (error: HttpErrorResponse) => {
        if (error.status === 404) {
          this._snack.open('Usuario o contraseña incorrectos', 'Cerrar', {
            duration: 2000,
          });
        } else {
          this._snack.open('Ha ocurrido un error con el servidor', 'Cerrar', {
            duration: 2000,
          });
        }
        console.log(error);

      });
  }
}
