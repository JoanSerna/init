import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [ './header.component.scss' ],
})
export class HeaderComponent implements OnInit {
  @Input() user;
  @Output() closeSession = new EventEmitter();
  @Output() ShowUser = new EventEmitter();
  @Output() showHelp = new EventEmitter<boolean>();

  public clickUser: boolean;
  public clickedButtonHelp: boolean;

  constructor () {
    this.clickUser = false;
    this.clickedButtonHelp = false;
    // this.logout.emit('Hello from child');
  }

  ngOnInit () {
  }

  logoutEvent () {
    const data = this.user;
    this.closeSession.emit(data);
    // this.closeSession.emit({nombre: 'joan'});
    // console.log('hla');
  }

  help () {
    this.clickedButtonHelp = !this.clickedButtonHelp;
    this.showHelp.emit(this.clickedButtonHelp);
  }

  userClicked () {
    this.clickUser = !this.clickUser;
    this.ShowUser.emit(this.clickUser);
  }
}
