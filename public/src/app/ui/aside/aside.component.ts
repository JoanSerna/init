import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: [ './aside.component.scss' ],
})
export class AsideComponent {
  @Input() rol: number;
  @Output() schedule = new EventEmitter();
  @Output() home = new EventEmitter();
  @Output() cancelar = new EventEmitter();
  @Output() Buscar = new EventEmitter();
  @Output() crearUser = new EventEmitter();
  @Output() crearGes = new EventEmitter();
  @Output() Reports = new EventEmitter();
  @Output() VerAgenda = new EventEmitter();
  @Output() Confi = new EventEmitter();

  public cambiar: boolean;

  constructor () {
    this.cambiar = false;
  }

  Agenda () {
    this.cambiar = !this.cambiar;
    this.VerAgenda.emit(this.cambiar);
  }

  goHome () {
    this.home.emit(true);
  }

  agenCita () {
    this.schedule.emit(true);
  }

  CancelarCita () {
    this.cancelar.emit(true);
  }

  buscarP () {
    this.Buscar.emit(true);
  }

  crearM () {
    this.crearUser.emit(true);
  }

  crearG () {
    this.crearGes.emit(true);
  }

  Reportes () {
    this.Reports.emit(true);
  }

  Config () {
    this.Confi.emit(true);
  }
}
