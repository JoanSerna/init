import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { MedicoService } from '../services/medico/medico.service';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: [ './medico.component.scss' ],
})
export class MedicoComponent implements OnInit {
    displayedColumns = ['select', 'position', 'name', 'weight', 'symbol'];
    dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
    selection = new SelectionModel<PeriodicElement>(true, []);
    public rol: string;
  public datas: object[];
  public userBoolean: boolean;
  public agenBoolean: boolean;
  public ocultarayuda: boolean;
  public showadjuntar: boolean;
  public medico: object[];
  private ELEMENT_DATA: any;


  constructor (private _svMedico: MedicoService, private _router: Router) {
    this.userBoolean = false;

    this.datas = [
      {
        text: 'Joseph Ochoa Gonzalez',
        icon: 'account_circle',
      },
      {
        text: 'jochoa59@hotmail.com',
        icon: 'email',
      },
      {
        text: '3214608816',
        icon: 'phone',
      },
      {
        text: '02/01/2000',
        icon: 'date_range',
      },
    ];

    this.medico = [];
  }

  logout (user) {
    sessionStorage.clear();
    this._router.navigate([ '/' ]);
  }

  ngOnInit () {
    this.medico = this._svMedico.getMedico();

    const typeRol = sessionStorage.getItem('key');
    this.rol = typeRol.substr(0, 1);
  }

  userEvent (event) {
    this.userBoolean = event;
    this.agenBoolean = false;
    this.ocultarayuda = false;
    this.showadjuntar = false;
  }

  ayudaEvent (event) {
    this.ocultarayuda = event;
    this.userBoolean = false;
    this.agenBoolean = false;
    this.showadjuntar = false;
  }

  MostrarAgenda (event) {
    this.agenBoolean = event;
    this.ocultarayuda = false;
    this.userBoolean = false;
    this.showadjuntar = false;
  }

  adjun (event) {
    this.showadjuntar = event;
    this.ocultarayuda = false;
    this.userBoolean = false;
    this.agenBoolean = false;
  }

  inicio (event) {
    this.agenBoolean = false;
    this.ocultarayuda = false;
    this.userBoolean = false;
    this.showadjuntar = false;
  }

  salir (event) {
    this.agenBoolean = false;
    this.showadjuntar = false;
    this.ocultarayuda = false;
  }
  /** subir imagen. */
    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

}


export interface PeriodicElement {
    name: string;
    position: number;
    weight: number;
    symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
    {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
    {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
    {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
