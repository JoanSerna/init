import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { LoginService } from '../services/login/login.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private _login: LoginService, private _router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (this._login.isLogged()) {
      return true;
    } else {
      this._router.navigate(['/']);
      return false;
    }
  }
}
