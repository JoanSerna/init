import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate
} from '@angular/router';
import { LoginService } from '../services/login/login.service';
import { Location } from '@angular/common';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(private _login: LoginService, private _location: Location) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (this._login.isLogged()) {
      this._location.back();
      return false;
    } else {
      return true;
    }
  }
}
