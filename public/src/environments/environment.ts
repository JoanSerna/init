// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCrUhvSP-i7M7KTHDAOa6BT0AKdCjRSsCk',
    authDomain: 'pycloudme-f6e22.firebaseapp.com',
    databaseURL: 'https://pycloudme-f6e22.firebaseio.com',
    projectId: 'pycloudme-f6e22',
    storageBucket: 'pycloudme-f6e22.appspot.com',
    messagingSenderId: '239139429422',
  },
  apiPycloudmeDev: 'http://localhost:8080/',
};
