<?php

namespace Pycloudme\Routes;

class Routing
{
    public function __construct()
    {
        if ($_SERVER['PATH_INFO'] = null) {
            echo json_encode(['Pycloudme Api Version 0.1']);
        }
    }

    /**
     * @param string $url
     * @param $controller
     */
    public function get(string $url, callable $controller): void
    {
        if ($_SERVER['PATH_INFO'] = !null) {
            if ($_SERVER['REQUEST_URI'] === "/${url}" && $_SERVER['REQUEST_METHOD'] === 'GET') {
                $get = json_encode($_GET);
                $controller($get);

            }
        }
    }

    /**
     * @param string $url
     * @param $controller
     */
    public function post(string $url, callable $controller): void
    {
        if ($_SERVER['PATH_INFO'] = !null) {
            if ($_SERVER['REQUEST_URI'] === "/${url}" && $_SERVER['REQUEST_METHOD'] ===
                'POST') {
                $post = (array)json_encode($_POST);
                if ($post[0] === '[]') {
                    $json = file_get_contents('php://input');
                    $post = json_decode($json);
                } else {
                    $json = (array)json_decode($post[0]);
                    $key = array_keys($json);
                    $post = json_decode($key[0]);
                }
                $controller($post);
            }
        }
    }

    /**
     * @param string $url
     * @param $controller
     */
    public function put(string $url, callable $controller): void
    {
        if ($_SERVER['PATH_INFO'] = !null) {
            if ($_SERVER['REQUEST_URI'] === "/${url}" && $_SERVER['REQUEST_METHOD'] ===
                'PUT') {
                $put = (array)json_encode($_POST);
                if ($put[0] === '[]') {
                    $json = file_get_contents('php://input');
                    $put = json_decode($json);
                } else {
                    $json = (array)json_decode($put[0]);
                    $key = array_keys($json);
                    $put = json_decode($key[0]);
                }
                $controller($put);
            }
        }
    }

    /**
     * @param string $url
     * @param $controller
     */
    public function delete(string $url, callable $controller): void
    {

        if ($_SERVER['PATH_INFO'] = !null) {
            if ($_SERVER['REQUEST_URI'] === "/${url}" && $_SERVER['REQUEST_METHOD'] ===
                'DELETE') {
                $delete = (array)json_encode($_POST);
                if ($delete[0] === '[]') {
                    $json = file_get_contents('php://input');
                    $delete = json_decode($json);
                } else {
                    $json = (array)json_decode($delete[0]);
                    $key = array_keys($json);
                    $delete = json_decode($key[0]);
                }
                $controller($delete);
            }
        }
    }
}