<?php
/**
 * Created by PhpStorm.
 * User: joanserna
 * Date: 12/07/18
 * Time: 04:57 PM
 */

namespace Pycloudme\Model;


class Configuracion
{
  public $id;
  public $empresa_ips_id;
  public $created_at;
  public $updated_at;
  public $deleted_at;
  public $duracion_citageneral;
  public $duracion_cita_especialista;
  public $duraccion_cita_laboratorio;
  public $hora_inicio;
  public $ultima_hora;

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   * @return Configuracion
   */
  public function setId($id): Configuracion
  {
    $this->id = $id;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getEmpresaIpsId()
  {
    return $this->empresa_ips_id;
  }

  /**
   * @param mixed $empresa_ips_id
   * @return Configuracion
   */
  public function setEmpresaIpsId($empresa_ips_id): Configuracion
  {
    $this->empresa_ips_id = $empresa_ips_id;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * @param mixed $created_at
   * @return Configuracion
   */
  public function setCreatedAt($created_at): Configuracion
  {
    $this->created_at = $created_at;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * @param mixed $updated_at
   * @return Configuracion
   */
  public function setUpdatedAt($updated_at): Configuracion
  {
    $this->updated_at = $updated_at;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getDeletedAt()
  {
    return $this->deleted_at;
  }

  /**
   * @param mixed $deleted_at
   * @return Configuracion
   */
  public function setDeletedAt($deleted_at): Configuracion
  {
    $this->deleted_at = $deleted_at;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getDuracionCitageneral()
  {
    return $this->duracion_citageneral;
  }

  /**
   * @param mixed $duracion_citageneral
   * @return Configuracion
   */
  public function setDuracionCitageneral($duracion_citageneral): Configuracion
  {
    $this->duracion_citageneral = $duracion_citageneral;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getDuracionCitaEspecialista()
  {
    return $this->duracion_cita_especialista;
  }

  /**
   * @param mixed $duracion_cita_especialista
   * @return Configuracion
   */
  public function setDuracionCitaEspecialista($duracion_cita_especialista): Configuracion
  {
    $this->duracion_cita_especialista = $duracion_cita_especialista;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getDuraccionCitaLaboratorio()
  {
    return $this->duraccion_cita_laboratorio;
  }

  /**
   * @param mixed $duraccion_cita_laboratorio
   * @return Configuracion
   */
  public function setDuraccionCitaLaboratorio($duraccion_cita_laboratorio): Configuracion
  {
    $this->duraccion_cita_laboratorio = $duraccion_cita_laboratorio;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getHoraInicio()
  {
    return $this->hora_inicio;
  }

  /**
   * @param mixed $hora_inicio
   * @return Configuracion
   */
  public function setHoraInicio($hora_inicio): Configuracion
  {
    $this->hora_inicio = $hora_inicio;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getUltimaHora()
  {
    return $this->ultima_hora;
  }

  /**
   * @param mixed $ultima_hora
   * @return Configuracion
   */
  public function setUltimaHora($ultima_hora): Configuracion
  {
    $this->ultima_hora = $ultima_hora;
    return $this;
  }

}
