<?php
/**
 * Created by PhpStorm.
 * User: joanserna
 * Date: 28/06/18
 * Time: 04:18 PM
 */

namespace Pycloudme\Model;


class Usuario
{
  public $id;
  public $rol_id;
  public $tpd_document_id;
  public $empresa_ips_id;
  public $nickname;
  public $number_document;
  public $password;
  public $actived;
  public $created_at;
  public $updated_at;
  public $deleted_at;

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   * @return Usuario
   */
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getRolId()
  {
    return $this->rol_id;
  }

  /**
   * @param mixed $rol_id
   * @return Usuario
   */
  public function setRolId($rol_id)
  {
    $this->rol_id = $rol_id;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getTpdDocumentId()
  {
    return $this->tpd_document_id;
  }

  /**
   * @param mixed $tpd_document_id
   * @return Usuario
   */
  public function setTpdDocumentId($tpd_document_id)
  {
    $this->tpd_document_id = $tpd_document_id;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getEmpresaIpsId()
  {
    return $this->empresa_ips_id;
  }

  /**
   * @param mixed $empresa_ips_id
   * @return Usuario
   */
  public function setEmpresaIpsId($empresa_ips_id)
  {
    $this->empresa_ips_id = $empresa_ips_id;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getNickname()
  {
    return $this->nickname;
  }

  /**
   * @param mixed $nickname
   * @return Usuario
   */
  public function setNickname($nickname)
  {
    $this->nickname = $nickname;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getNumberDocument()
  {
    return $this->number_document;
  }

  /**
   * @param mixed $number_document
   * @return Usuario
   */
  public function setNumberDocument($number_document)
  {
    $this->number_document = $number_document;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * @param mixed $password
   * @return Usuario
   */
  public function setPassword($password)
  {
    $this->password = $password;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getActived()
  {
    return $this->actived;
  }

  /**
   * @param mixed $actived
   * @return Usuario
   */
  public function setActived($actived)
  {
    $this->actived = $actived;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * @param mixed $created_at
   * @return Usuario
   */
  public function setCreatedAt($created_at)
  {
    $this->created_at = $created_at;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * @param mixed $updated_at
   * @return Usuario
   */
  public function setUpdatedAt($updated_at)
  {
    $this->updated_at = $updated_at;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getDeletedAt()
  {
    return $this->deleted_at;
  }

  /**
   * @param mixed $deleted_at
   * @return Usuario
   */
  public function setDeletedAt($deleted_at)
  {
    $this->deleted_at = $deleted_at;
    return $this;
  }
}
