<?php

namespace Pycloudme\Model;

use Pycloudme\Config\Config;

class Connexion
{
  protected $pdo;

  /**
   * Connexion constructor.
   */
  public function __construct()
  {
    $config = new Config();
    try {
      $this->pdo = new \PDO("{$config->getDriver()}:dbname={$config->getDbname()};host={$config->getHost()};port={$config->getPort()}", (string)$config->getUser(), (string)$config->getPassword());
      $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
      $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
    } catch (\PDOException $error) {
      echo " {$error->getTrace()} " . PHP_EOL . " {$error->getFile()} " . PHP_EOL . " {$error->getLine()}" . PHP_EOL . " {$error->getCode()} "
        . PHP_EOL . " {$error->getMessage()} ";
    }
  }

  /**
   * Return one string encode in json with all properties of model
   *
   * @param object $model
   * @param string $table
   *
   * @return array
   */
  public function selectAll(object $model, string $table): array
  {
    $properties = (array)$model;
    $propertiesColumn = '';
    foreach ($properties as $key => $value) {
      $propertiesColumn .= "{$key}, ";
    }
    $propertiesColumn = substr_replace($propertiesColumn, '', \strlen($propertiesColumn) * 1 - 2);
    $stm = $this->pdo->query("SELECT {$propertiesColumn} FROM $table");
    $stm->execute();
    return $stm->fetchAll();
  }

  /**
   * Insert one resource
   *
   * @param $model
   * @param $table
   *
   * @return bool|null
   */
  public function insert(object $model, string $table): ?bool
  {
    $properties = (array)$model;
    $column = '';
    $value = '';
    foreach ($properties as $col => $val) {
      if ($col !== 'id') {
        $column .= "${col},";
        $value .= ":${col},";
      }
    }
    $column = substr($column, 0, -1);
    $value = substr($value, 0, -1);
    try {
      $this->pdo->beginTransaction();
      $stm = $this->pdo->prepare("INSERT INTO {$table} ($column) VALUES ($value)");
      foreach ($properties as $parameter => $valueParameter) {
        if ($parameter !== 'id') {
          $stm->bindValue(':' . $parameter, $valueParameter);
        }
      }
      $stm->execute();
      $this->pdo->commit();
      return true;
    } catch (\PDOException $e) {
      $this->pdo->rollBack();
      echo $e->getMessage();
      echo $e->getTrace();
      echo $e->getCode();
      echo $e->getLine();
      return false;
    }
  }

  public function update(object $model, string $table): bool
  {
    $properties = (array)$model;
    $column = '';
    $value = '';
    foreach ($properties as $col => $val) {
      if ($col !== 'id') {
        $column .= "${col}=:${col},";
        $value .= ":${col},";
      }
    }
    $column = substr($column, 0, -1);
    try {
      $this->pdo->beginTransaction();
      $stm = $this->pdo->prepare("UPDATE {$table} SET {$column} WHERE id=:id");
      foreach ($properties as $parameter => $valueParameter) {
        $stm->bindValue(':' . $parameter, $valueParameter);
      }
      $stm->execute();
      $this->pdo->commit();
      return true;
    } catch (\PDOException $e) {
      $this->pdo->rollBack();
      echo $e->getMessage();
      echo $e->getCode();
      echo $e->getLine();
      return false;
    }
  }

  public function delete(object $model, string $table): ?bool
  {
    $properties = (array)$model;
    try {
      $this->pdo->beginTransaction();
      $stm = $this->pdo->prepare("DELETE FROM {$table} WHERE id=:id");
      $stm->bindValue(':id', $properties['id']);
      $stm->execute();
      $this->pdo->commit();
      return true;
    } catch (\PDOException $e) {
      $this->pdo->rollBack();
      echo $e->getMessage();
      echo $e->getCode();
      echo $e->getLine();
      return false;
    }
  }

  public function where($model, $table, $column, $equal): string
  {
    $properties = (array)$model;
    $propertiesColumn = '';
    foreach ($properties as $key => $value) {
      $propertiesColumn .= "{$key}, ";
    }
    $propertiesColumn = substr_replace($propertiesColumn, '', \strlen($propertiesColumn) * 1 - 2);
    $stm = $this->pdo->prepare("SELECT {$propertiesColumn} FROM $table WHERE {$column} = :equal");
    $stm->execute([':equal' => $equal]);
    return json_encode($stm->fetchAll());
  }

  /**
   * @param $query
   * @return array|string
   */
  public function sql($query)
  {
    try {
      $this->pdo->beginTransaction();
      $stm = $this->pdo->query($query);
      $stm->execute();
      $this->pdo->commit();
      return $stm->fetchAll();
    } catch (\PDOException $PDOException) {
      $this->pdo->rollBack();
      return $PDOException->getMessage();
    }

  }

  /**
   * @param $model
   * @param $table
   * @param $numberDocument
   * @param $usuario
   * @param $password
   * @param $tpd_document
   * @return string
   */
  public function auth($model, $table, $numberDocument, $usuario, $password, $tpd_document): string
  {
    $properties = (array)$model;
    $propertiesColumn = '';
    foreach ($properties as $key => $value) {
      $propertiesColumn .= "{$key}, ";
    }
    $propertiesColumn = substr_replace($propertiesColumn, '', \strlen($propertiesColumn) * 1 - 2);
    try {
      $this->pdo->beginTransaction();
      $stm = $this->pdo->prepare("SELECT {$propertiesColumn} FROM $table WHERE nickname = :usuario AND password = :password2 AND tpd_documento_id = :tpd_document AND number_doc = :numberDoc");
      $stm->bindValue(':usuario', $usuario);
      $stm->bindValue(':password2', $password);
      $stm->bindValue(':tpd_document', $tpd_document);
      $stm->bindValue(':numberDoc', $numberDocument);
      $stm->execute();
      $this->pdo->commit();
      return json_encode($stm->fetchAll());
    } catch (\PDOException $exception) {
      $this->pdo->rollBack();
      echo $exception->getMessage();
      echo $exception->getCode();
      echo $exception->getLine();
      return false;
    }
  }
}
