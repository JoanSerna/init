<?php

namespace Pycloudme\Controller;

use Pycloudme\Model\Configuracion;
use Pycloudme\Model\Connexion;

class ConfiguracionController
{
  public function add_config_system($request): void
  {
    $duracion_cita_especialista = $this->convertToTime($request->duracion_cita_especialista);
    $duracion_cita_laboratorio = $this->convertToTime($request->duracion_cita_laboratorio);
    $duracion_citageneral = $this->convertToTime($request->duracion_citageneral);
    $connexion = new Connexion();
    $configuracion = new Configuracion();
    $configuracion->setDuracionCitaEspecialista($duracion_cita_especialista);
    $configuracion->setDuraccionCitaLaboratorio($duracion_cita_laboratorio);
    $configuracion->setDuracionCitageneral($duracion_citageneral);
    $configuracion->setEmpresaIpsId($request->empresa_ips_id);
    $configuracion->setHoraInicio($request->hora_inicio);
    $configuracion->setUltimaHora($request->ultima_hora);
    $configuracion->setCreatedAt(date('Y-m-d H:i:s'));
    $sql = $connexion->insert($configuracion, 'configuracion');
    if ($sql) {

      echo json_encode(['data' => '', 'message' => 'Se ha configurado correctamente el sistema']);
    } else {
      echo json_encode(['data' => '', 'message' => 'Ha ocurrido un error al configurar el sistema']);
    }
  }

  /**
   * @param string $minutes
   * @return string
   */
  private function convertToTime(string $minutes): ?string
  {
    if ((integer)$minutes > 60) {
      $hour = (integer)$minutes / 60;
      if (!strpos($hour, '.')) {
        return "0{$hour}:00";
      }
      $hour = substr($hour, 0, strpos($hour, '.'));
      $minus = (integer)$minutes - ((int)$hour * 60);
      return "0{$hour}:$minus";
    }
    return "00:{$minutes}";
  }
}
