<?php

namespace Pycloudme\Controller;


use Pycloudme\Model\Connexion;
use Pycloudme\Model\Usuario;

class AuthController
{
  /**
   * @param $request
   */
  public function auth($request)
  {
    $model = new Usuario();
    $conexion = new Connexion();
    $user = $conexion->auth($model, 'usuario', $request->numberDocument, $request->username, $request->password,
      $request->typeDocument);
    echo $user;
  }
}
