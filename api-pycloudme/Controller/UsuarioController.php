<?php

namespace Pycloudme\Controller;

use Pycloudme\Model\Connexion;
use Pycloudme\Model\Usuario;

class UsuarioController
{
  /**
   *
   */
  public function getUsers(): void
  {
    $user = new Usuario();
    $connexion = new Connexion;
    $data = $connexion->selectAll($user, 'usuario');
    echo json_encode($data);
  }

  /**
   * @param $request
   */
  public function authUser($request): void
  {
    $user = new Usuario();
    $connexion = new Connexion;
    $data = $connexion->selectAll($user, 'usuario');
    $exist = false;
    $user_Logged = [];
    foreach ($data as $user) {
      if ($user->tpd_document_id === $request->tpd_document_id && (int)$user->number_document ===
        (int)$request->number_document
        && $request->nickname === $user->nickname && password_verify($request->password, $user->password)) {
        $exist = true;
        unset($user->password);
        $user_Logged = $user;
      }
    }
    if ($exist) {
      switch ($user_Logged->rol_id) {
        case 1:
          $datoUsuario = $connexion->sql(
            "SELECT * FROM usuario_dato_sistema INNER JOIN usuario on usuario_dato_sistema.usuario_id = usuario.id WHERE usuario_id = {$user_Logged->id}");
          unset($datoUsuario[0]->password);
          http_response_code(200);
          echo json_encode(['data' => $datoUsuario[0], 'message' => '']);
          break;
        case 2:
          $datoUsuario = $connexion->sql(
            "SELECT * FROM usuario_dato_sistema INNER JOIN usuario on usuario_dato_sistema.usuario_id = usuario.id WHERE usuario_id = {$user_Logged->id}");
          unset($datoUsuario[0]->password);
          http_response_code(200);
          echo json_encode(['data' => $datoUsuario[0], 'message' => '']);
          break;
        case 3:
          echo json_encode(['data' => 'Medico', 'message' => '']);
          break;
        case 4:
          echo json_encode(['data' => 'Paciente', 'message' => '']);

          break;
        default:
          break;
      }
    } else {
      http_response_code(404);
      echo json_encode(['data' => '', 'message' => 'Usuario o contraseña incorrectos']);
    }
  }
}
