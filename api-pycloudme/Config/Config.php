<?php

namespace Pycloudme\Config;
class Config
{
    private $driver;
    private $host;
    private $dbname;
    private $user;
    private $password;
    private $port;

    public function __construct()
    {
        $config = $this->loadConfig();
        $this->setDriver($config->driver);
        $this->setHost($config->host);
        $this->setDbname($config->dbname);
        $this->setUser($config->user);
        $this->setPassword($config->password);
        $this->setPort($config->port);

    }

    private function loadConfig()
    {
        $config = [];
        $data = file_get_contents('./../.env');
        if ($data) {
            $data = explode(PHP_EOL, $data);
            array_pop($data);
            for ($i = 0, $iMax = \count($data); $i < $iMax; $i++) {
                $element = explode('=', $data[$i]);
                $config[(string)$element[0]] = $element[1];
            }
            return (object)$config;
        }
        echo json_encode('No se pudo encontrar el archivo de configuracion');
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     *
     * @return Config
     */
    public function setHost($host): Config
    {
        $this->host = $host;
        return $this;
    }

    /**
     * Get the value of driver
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Set the value of driver
     *
     * @param string $driver
     *
     * @return  self
     */
    public function setDriver(string $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get the value of dbname
     */
    public function getDbname()
    {
        return $this->dbname;
    }

    /**
     * Set the value of dbname
     *
     * @param string $dbname
     *
     * @return  self
     */
    public function setDbname(string $dbname): self
    {
        $this->dbname = $dbname;

        return $this;
    }

    /**
     * Get the value of user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @param string $user
     *
     * @return  self
     */
    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @param string $password
     *
     * @return  self
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of port
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set the value of port
     *
     * @param int $port
     *
     * @return  self
     */
    public function setPort(int $port): self
    {
        $this->port = $port;

        return $this;
    }
}
