<?php

namespace Pycloudme\View;

class Response
{
  public function json($data, int $code): void
  {
    header('Content-Type: application/json', true ,$code);
    echo json_encode($data);
  }
}
