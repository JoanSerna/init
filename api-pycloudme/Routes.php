<?php

use Pycloudme\Controller\ConfiguracionController;
use Pycloudme\Controller\UsuarioController;
use Pycloudme\Routes\Routing;

$routing = new Routing();

/**
 * Declaration Routes
 */
$routing->get('', function () {
  echo 'Api Pycloudme version 0.1' . PHP_EOL;
  echo password_hash('123456', PASSWORD_ARGON2I);
});

$routing->get('usuarios', function () {
  $usuarioController = new UsuarioController();
  $usuarioController->getUsers();
});

$routing->post('auth', function ($request) {
  $usuarioController = new UsuarioController();
  $usuarioController->authUser($request);
});

$routing->post('config', function ($request) {
  $configuracionController = new ConfiguracionController();
  $configuracionController->add_config_system($request);
});

$routing->delete('pokemons', function ($request) {
  echo json_encode($request);
});


























